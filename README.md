# laravel docker

# Usage
1. `git clone`
2. `composer create-project laravel/laravel your-project-name`
3. 修改 .env： WORKSPACES=./your-project-name
4. 修改 laravel 应用更目录 的 .env 文件
    ```
    DB_HOST=mysql
    REDIS_HOST=redis
    ```
 1. `docker-compose up -d`